// const paragraphs = document.querySelectorAll('p');

// paragraphs.forEach((elem) => {
//     elem.style.backgroundColor = '#ff0000'
// })

// const optionsList = document.getElementById("optionsList")


// for (let i = 0; i < optionsList.childNodes.length; i++) {
//     console.log(optionsList.childNodes[i]);
//     console.log(optionsList.childNodes[i].nodeType);
// }

// console.log(optionsList);
// console.log(optionsList.parentNode);
// console.log(optionsList.childNodes);


// const testParagraph = document.querySelector('#testParagraph');

// testParagraph.innerHTML = 'This is a paragraph'

// console.log(testParagraph);

// const elemMainheader = document.querySelectorAll('.main-header li');

// elemMainheader.forEach((elem) => {
//     elem.classList.add('nav-item');
// })

// console.log(elemMainheader);


// const elemSectionTitle = document.querySelectorAll('.section-title');

// elemSectionTitle.forEach((elem) => {
//     elem.classList.remove('section-title');
// })

// console.log(elemSectionTitle);



const arr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];

// вариант 1:
// const showElement = (array, parent = document.body) => {
    
//     const fragment = new DocumentFragment();
    
//     // const container = document.createElement('div');
//     // container.className = "container";
//     const list = document.createElement('ul');
//     list.className = "list";

//     array.forEach((el) => {
//         const li = document.createElement('li');
//         li.innerText = el;
//         list.append(li)
//     }) 

//     // container.append(list);
//     // parent.append(container);

//     fragment.append(list);
//     parent.append(fragment);
// }

// showElement(arr)


// вариант 2:
// const showElement = (array, parent = document.body) => {
    
//     const container = document.createElement('div');
//     container.className = "container";
//     parent.append(container);
//     const list = document.createElement('ul');
//     list.className = "list";
//     container.append(list);

//     // array.forEach((el) => {
//     //     list.insertAdjacentHTML('beforeend', `<li>${el}</li>`)
//     // }) 

//     const listHtml = array.map((el) => {
//         return`<li>${el}</li>`
//     })
//     list.insertAdjacentHTML("beforeend", listHtml.join(''));
// }

// showElement(arr)


const showElement = (array, parent = document.body) => {
    
    const list = document.createElement('ul');
    list.className = "list";
    parent.append(list);

    const listHtml = array.map((el) => {

        const li = document.createElement('li');

        if (Array.isArray(el)) {
            li.append(showElement(el));
        } else {
            li.innerText = el;
        }

        return list.append(li);
    })

    return list

}

showElement(arr)